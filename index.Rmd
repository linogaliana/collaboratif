--- 
title: "Travail collaboratif avec R"
author: "Lino Galiana"
date: "`r Sys.Date()`"
knit: "bookdown::render_book"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "Support associé à la formation Travail collaboratif avec R"
---

# Introduction

Supports associés à la formation nationale `Travail collaboratif avec R`. `r colorize("Version provisoire et non définitive des supports", "red")`. Le code source est disponible sur Gitlab <i class="fab fa-gitlab"></i> [ici](https://gitlab.com/linogaliana/collaboratif) 

Pour pré-visualiser en local le résultat de la compilation, vous pouvez utiliser la commande suivante:

```{r, eval = FALSE}
bookdown::render_book('./index.Rmd', output_dir = 'public')
```

Cette formation est associée à un guide des bonnes disponible dans la documentation [utilitR](https://www.book.utilitr.org) que nous recommandons de consulter. 

```{r include=FALSE}
# automatically create a bib database for R packages
knitr::write_bib(c(
  .packages(), 'bookdown', 'knitr', 'rmarkdown'
), 'packages.bib')
```

## Contributeurs {-}

* Chapitre \@ref(rstudio): [Lino Galiana](https://linogaliana.netlify.com/)
* Chapitres \@ref(git) et \@ref(gitexo): [Mathias André](https://gitlab.com/mathias.andre), [Lino Galiana](https://linogaliana.netlify.com/), [Olivier Meslin](https://gitlab.com/oliviermeslin), 
* Chapitres \@ref(rmd) et \@ref(rmdexo): [Annie Moineau](https://gitlab.com/annieMoineau), [Lino Galiana](https://linogaliana.netlify.com/), [Olivier Meslin](https://gitlab.com/oliviermeslin)
* Chapitre \@ref(fonctions):  [Lino Galiana](https://linogaliana.netlify.com/)
* Chapitre \@ref(package):  [Romain Lesur](https://github.com/RLesur)

Relecteurs: [Olivier Meslin](https://gitlab.com/oliviermeslin), George Pavlov, [Lino Galiana](https://linogaliana.netlify.com/), [Romain Lesur](https://github.com/RLesur)

Mise en forme: [Lino Galiana](https://linogaliana.netlify.com/), [Romain Lesur](https://github.com/RLesur)
